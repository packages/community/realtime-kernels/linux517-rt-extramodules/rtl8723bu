# Maintainer: Bernhard Landauer <bernhard@manjaro.org>
# Maintainer: Philip Müller <philm[at]manjaro[dot]org>
# Archlinux credits:
# Massimiliano Torromeo <massimiliano.torromeo@gmail.com>
# Bob Fanger < bfanger(at)gmail >
# Filip <fila pruda com>, Det < nimetonmaili(at)gmail >

_linuxprefix=linux517-rt
_extramodules=extramodules-5.17-rt-MANJARO
_pkgname=rtl8723bu
pkgname=$_linuxprefix-$_pkgname
_libname=8723bu
_commit=19b4bdc05483a1e79a4f955a80f8f72cdd5dedc3
pkgver=20220114
pkgrel=4
pkgdesc="A kernel module for Realtek 8723bu network cards"
url="http://www.realtek.com.tw"
license=("GPL")
arch=('x86_64')
depends=("$_linuxprefix")
makedepends=("$_linuxprefix-headers")
builddepends=("$_linuxprefix-headers")
provides=("$_pkgname=$pkgver")
replaces=("linux516-rt-$_pkgname")
groups=("$_linuxprefix-extramodules")
source=("${_pkgname}-${pkgver}.zip::https://github.com/lwfinger/rtl8723bu/archive/$_commit.zip"
        "blacklist-rtl8xxxu.conf")
sha256sums=('0f4948b76dd8f030ef6957b8e0891bd01b2a1072fae11e94a7857bf15f07ca8b'
            '7c726ad04083c8e620bc11c837e5f51d3e9e2a5c3e19c333b2968eb39f1ef07e')
install=rtl8723bu.install

build() {
    _kernver="$(cat /usr/lib/modules/$_extramodules/version || true)"
    cd "$_pkgname-$_commit"
    # do not compile with CONCURRENT_MODE
    sed -i 's/EXTRA_CFLAGS += -DCONFIG_CONCURRENT_MODE/#EXTRA_CFLAGS += -DCONFIG_CONCURRENT_MODE/g' Makefile

    # avoid using the Makefile directly -- it doesn't understand
    # any kernel but the current.
    make clean
    make -C /usr/lib/modules/$_kernver/build M="$srcdir/$_pkgname-$_commit" modules
}

package() {
    install -D -m 644 "blacklist-rtl8xxxu.conf" "${pkgdir}/etc/modprobe.d/${_linuxprefix}-blacklist-rtl8xxxu.conf"

    install -D -m644 "$_pkgname-$_commit/$_libname.ko" "$pkgdir/usr/lib/modules/$_extramodules/$_libname.ko"

    # set the kernel we've built for inside the install script
    sed -i -e "s/EXTRAMODULES=.*/EXTRAMODULES=${_extramodules}/g" "${startdir}/${install}"

    find "$pkgdir" -name '*.ko' -exec gzip -9 {} \;
}
